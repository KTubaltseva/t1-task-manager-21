package ru.t1.ktubaltseva.tm;

import ru.t1.ktubaltseva.tm.component.Bootstrap;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class Application {

    public static void main(String[] args) throws AbstractException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}