package ru.t1.ktubaltseva.tm.command.task;

import ru.t1.ktubaltseva.tm.api.service.IProjectTaskService;
import ru.t1.ktubaltseva.tm.api.service.ITaskService;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            System.out.println();
            index++;
        }
    }

    public void displayTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESC: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    protected void renderTasksFullInfo(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ".");
            displayTask(task);
            System.out.println();
            index++;
        }
    }

}