package ru.t1.ktubaltseva.tm.command.project;

import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectDisplayListFullInfoCommand extends AbstractProjectCommand {

    private final String NAME = "project-list-full-info";

    private final String DESC = "Display project list (full info).";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AuthRequiredException {
        System.out.println("[DISPLAY PROJECTS FULL INFO]");
        System.out.println("[ENTER SORT]:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(getUserId(), sort);
        renderProjectsFullInfo(projects);
    }

}
