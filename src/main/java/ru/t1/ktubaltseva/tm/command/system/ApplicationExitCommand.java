package ru.t1.ktubaltseva.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    private final String NAME = "exit";
    private final String DESC = "Close Application.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

}
