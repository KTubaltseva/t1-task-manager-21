package ru.t1.ktubaltseva.tm.command.project;

import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class ProjectDisplayByIndexCommand extends AbstractProjectCommand {

    private final String NAME = "project-display-by-index";

    private final String DESC = "Display project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Project project = getProjectService().findOneByIndex(getUserId(), index);
        displayProject(project);
    }

}
