package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.ITaskService;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.DescriptionEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(ITaskRepository modelRepository) {
        super(modelRepository);
    }

    @Override
    public Task create(
            final String userId,
            final String name,
            final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return modelRepository.create(userId, name, description);
    }

    @Override
    public Task create(final String userId, final String name) throws NameEmptyException, AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return modelRepository.create(userId, name);
    }

    @Override
    public Task changeTaskStatusById(
            final String userId,
            final String id,
            final Status status
    ) throws AbstractException {
        Task task;
        try {
            task = findOneById(userId, id);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(
            final String userId,
            final Integer index,
            final Status status
    ) throws AbstractException {
        Task task;
        try {
            task = findOneByIndex(userId, index);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAll(final String userId, final Sort sort) throws AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (sort == null) return findAll(userId);
        return modelRepository.findAll(userId, sort.getComparator());
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, String projectId) throws AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return modelRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task updateById(
            final String userId,
            final String id,
            final String name,
            final String description
    ) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Task task;
        try {
            task = findOneById(userId, id);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(
            final String userId,
            final Integer index,
            final String name,
            final String description
    ) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Task task;
        try {
            task = findOneByIndex(userId, index);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
