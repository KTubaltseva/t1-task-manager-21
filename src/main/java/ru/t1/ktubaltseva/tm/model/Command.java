package ru.t1.ktubaltseva.tm.model;

import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public final class Command extends AbstractCommand {

    private String name = null;

    private String argument = null;

    private String description = null;

    public Command() {
    }

    public Command(final String name, final String argument, final String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getArgument() {
        return argument;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() throws AbstractException {
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
