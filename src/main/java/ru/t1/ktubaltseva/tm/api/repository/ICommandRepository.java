package ru.t1.ktubaltseva.tm.api.repository;

import ru.t1.ktubaltseva.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    AbstractCommand getCommandByArgument(String argument);

    AbstractCommand getCommandByName(String name);

    Collection<AbstractCommand> getTerminalCommands();
}
